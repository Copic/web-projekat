﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Buyer : User
    {
        public int Points { get; set; }        
        public UserType UserType { get; set; }
        public List<int> MyReservations { get; set; } = new List<int>();

        public Buyer() { }

        public Buyer(string[] parts) : base(parts)
        {
            Points = Int32.Parse(parts[8]);
            UserType = new UserType() { Points = 10, Discount = Int32.Parse(parts[9]), UserTypeEnum = GetUserType(parts[10]) };
            //for (int i = 11; i < parts.Length; i++)
            //{
            //    MyReservations.Add(Int32.Parse(parts[i]));
            //}
        }

        public override string ToString()
        {            
                return $"{Username};{Password};{Name};{LastName};{GetGenderString(Gender)};{Birthday.ToString("dd-MM-yyyy")};{GetRoleString(Role)};{IsDeleted.ToString()};{Points};{UserType.Discount.ToString()};{UserType.UserTypeEnum.ToString()}";
        }

        public override void AppendToFile(string fileName)
        {
            var path = Constants.DATA_ROOT_PATH + fileName;
            File.AppendAllText(path, this.ToString() + Environment.NewLine);
        }

        public UserTypeEnum GetUserType(string usertype)
        {
            switch (usertype.ToLower())
            {
                case "golden":
                    return UserTypeEnum.Golden;
                case "silver":
                    return UserTypeEnum.Silver;
                default:
                    return UserTypeEnum.Bronze;
            }
        }

        public void RetrieveMyReservations()
        {
            foreach(Reservation r in Models.AllTickets.Reservations)
            {
                if(r.Buyer.Username == Username)
                {
                    MyReservations.Add(r.Id);
                }
            }
        }

        public Reservation GetReservation(int id)
        {
            foreach (Reservation r in AllTickets.Reservations)
            {
                if (r.Id == id)
                    return r;
            }

            return null;
        }
    }
}