﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public static class SubArray
    {
        /// <summary>
        ///  Gets subarray from index, to the end. (including element at index)
        /// </summary>
        /// <param name="data"></param>
        /// <param name="index"></param>
        public static string[] GetSubArray(string[] data, int index)
        {
            var newLength = data.Length - index;
            var result = new string[newLength];
            Array.Copy(data, index, result, 0, newLength);
            return result;
        }
    }
}