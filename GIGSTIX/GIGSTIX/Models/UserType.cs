﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class UserType
    {
        public UserTypeEnum UserTypeEnum { get; set; }
        public int Discount { get; set; }
        public int Points { get; set; }

        public UserType() { }
    }
}