﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using GIGSTIX.Models;

namespace GIGSTIX.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public DateTime Birthday { get; set; }
        public Role Role { get; set; }
        public bool IsLogIn { get; set; }
        public bool IsDeleted { get; set; }

        public User() { }
        
        /// <param name="parts">Username, Password, Name, LastName, Gender, Birthday, Role</param>
        public User(string[] parts)
        {
            Username = parts[0];
            Password = parts[1];
            Name = parts[2];
            LastName = parts[3];
            Gender = GetGender(parts[4]);
            Birthday = DateTime.ParseExact(parts[5], "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Role = GetRole(parts[6]);
            IsDeleted = parts[7] == "True" ? true : false;
            IsLogIn = false;
        }

        public static Role GetRole(string role)
        {
            switch (role.ToLower())
            {
                case "admin":
                    return Role.Admin;
                case "buyer":
                    return Role.Buyer;
                case "seller":
                    return Role.Seller;
                default:
                    return Role.Admin;
            }
        }

        public static string GetRoleString(Role role)
        {
            switch (role)
            {
                case Role.Admin:
                    return "admin";
                case Role.Buyer:
                    return "buyer";
                case Role.Seller:
                    return "seller";
                default:
                    return "buyer";
            }
        }

        public static Gender GetGender(string gender)
        {
            switch (gender.ToLower())
            {
                case "male":
                    return Gender.Male;
                case "female":
                    return Gender.Female;
                default:
                    return Gender.Other;
            }
        }

        public static string GetGenderString(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return "male";
                case Gender.Female:
                    return "female";
                case Gender.Other:
                    return "other";
                default:
                    return "other";
            }
        }

        public override string ToString()
        {
            return $"{Username};{Password};{Name};{LastName};{GetGenderString(Gender)};{Birthday.ToString("dd-MM-yyyy")};{GetRoleString(Role)};{IsDeleted.ToString()}";
        }

        public virtual void AppendToFile(string fileName)
        {
            var path = Constants.DATA_ROOT_PATH + fileName;
            File.AppendAllText(path, this.ToString() + Environment.NewLine);
        }

        ////////////////////////////////////////
        // Methods for LINQ queries.
        
        public bool IsName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return true;
            if (Name == name)
                return true;

            return false;
        }

        public bool IsLastName(string lastname)
        {
            if (string.IsNullOrEmpty(lastname))
                return true;

            if (LastName == lastname)
                return true;

            return false;
        }
                    
        public bool IsRole(string role)
        {
            if (string.IsNullOrEmpty(role))
                return true;

            var filter = GetRole(role);

            return Role == filter;
        }

        public bool IsGender(string gender)
        {
            if (string.IsNullOrEmpty(gender))
                return true;

            var filter = GetGender(gender);

            return Gender == filter;
        }

        public bool IsUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                return true;

            return Username == username;
        }
    }
}